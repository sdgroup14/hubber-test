var gulp = require('gulp'),
  svgSprite = require('gulp-svg-sprite'),
  svgmin = require('gulp-svgmin'),
  cheerio = require('gulp-cheerio'),
  replace = require('gulp-replace');


gulp.task('svg', function () {
  return gulp.src('svg-sprite/*.svg')
  // minify svg
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    // remove all fill, style and stroke declarations in out shapes
    .pipe(cheerio({
      run: function ($) {
        $('[fill]').removeAttr('fill');
        $('[stroke]').removeAttr('stroke');
        $('[style]').removeAttr('style');
        $('[class]').removeAttr('class');
        $('style').remove();
      },
      parserOptions: {xmlMode: true}
    }))
    // cheerio plugin create unnecessary string '&gt;', so replace it.
    .pipe(replace('&gt;', '>'))
    // build svg sprite
    .pipe(svgSprite({
      mode: {
        symbol: {
          sprite: "../../../src/assets/img/sprite.svg",
          render: {
            scss: {
              dest: '../../../src/assets/scss/libs/_sprite.scss',
              template: "svg-sprite/scss/sprite-template.scss"
            }
          }
        }
      }
    }))
    .pipe(gulp.dest('svg-sprite/done/'))
});



