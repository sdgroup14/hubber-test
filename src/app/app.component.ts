import {Component} from '@angular/core';
import {setTheme} from 'ngx-bootstrap';
import {NewsService} from './services/news.service';
import {CATEGORIES, COUNTRIES, DEFAULT_CATEGORY, DEFAULT_COUNTRY} from './components/select/selectsLists.data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  totalItems = 0;
  currentPage = 1;
  countries = [];
  categories = [];
  defaultCategory = DEFAULT_CATEGORY;
  defaultCountry = DEFAULT_COUNTRY;

  newPaginationData(data) {
    this.totalItems = data.total;
    setTimeout(() => {
      this.currentPage = data.current;
    }, 100);
  }

  changePage(pagination) {
    let params = this.service.queryParams.value;
    if (params.page !== pagination.page) {
      params = {...params, page: pagination.page};
      this.service.queryParams.next(params);
    }
  }

  constructor(private service: NewsService) {
    setTheme('bs4');
    this.categories = CATEGORIES;
    this.countries = COUNTRIES;
  }
}
