import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject, Subject} from 'rxjs';
import {DEFAULT_CATEGORY, DEFAULT_COUNTRY} from '../components/select/selectsLists.data';

const initParams = {
  country: DEFAULT_COUNTRY,
  category: DEFAULT_CATEGORY,
  apiKey: environment.apiKey,
  q: '',
  page: 1,
  pageSize: 5
};

@Injectable({
  providedIn: 'root'
})

export class NewsService {
  queryParams = new BehaviorSubject(initParams);
  news = new Subject();

  constructor(private http: HttpClient) {
    this.queryParams.subscribe(params => {
      this.getNews(params);
    });
  }

  getNews(params) {
    return this.http.get(environment.apiUrl + '/top-headlines', {
      params: {...params}
    }).subscribe(data => {
      this.news.next(data);
    });

  }
}
