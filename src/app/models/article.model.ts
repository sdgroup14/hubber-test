export class Article {
  urlToImage: string;
  title: string;
  author: string;
  description: string;
  publishedAt: string;

  constructor(
    urlToImage: string,
    title: string,
    author: string,
    description: string,
    publishedAt: string
  ) {
    this.urlToImage = urlToImage ? urlToImage : '/assets/img/default.png';
    this.title = title;
    this.author = author ? author : 'Unknown';
    this.description = description;
    this.publishedAt = publishedAt;

  }
}
