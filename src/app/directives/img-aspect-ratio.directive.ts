import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appImgAspectRatio]'
})
export class ImgAspectRatioDirective {

  constructor(private el: ElementRef) {
  }

  @HostListener('load') onLoad() {
    const el = this.el.nativeElement;
    const width = el.naturalWidth;
    const height = el.naturalHeight;
    if (width > height) {
      el.style.height = '100%';
    } else {
      el.style.width = '100%';
    }
  }
}
