import {Component, Input, OnInit} from '@angular/core';
import {NewsService} from '../../services/news.service';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  @Input() items = [];
  @Input() key: string;
  @Input() default: string;
  value = '';

  changeValue(value) {
    const params = {...this.service.queryParams.value, page: 1};
    params[this.key] = value ? value : this.default;
    this.service.queryParams.next(params);
  }

  constructor(private service: NewsService) {
  }

  ngOnInit() {
    this.value = this.default;
  }

}
