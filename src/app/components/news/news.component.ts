import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NewsService} from '../../services/news.service';
import {Article} from '../../models/article.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  articles: Article[] = [];
  @Output() changePagination = new EventEmitter();

  constructor(private service: NewsService) {
  }

  ngOnInit() {
    this.service.news.subscribe((resp: any) => {
      this.articles = [];
      resp.articles.map(article => {
        this.articles.push(new Article(
          article.urlToImage,
          article.title,
          article.author,
          article.description,
          article.publishedAt,
        ));
      });
      this.changePagination.emit({
        total: resp.totalResults,
        current: this.service.queryParams.value.page
      });
    });
  }

}
