import {Component, OnInit} from '@angular/core';
import {NewsService} from '../../services/news.service';
import {FormControl} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchControl = new FormControl();
  checkClear = true;

  constructor(private service: NewsService) {
  }

  ngOnInit() {
    this.searchControl.valueChanges.pipe(
      debounceTime(400)
    ).subscribe(newValue => {
      const params = {...this.service.queryParams.value, page: 1};
      params.q = newValue;
      if (newValue.length > 2) {
        this.checkClear = false;
        this.service.queryParams.next(params);
      } else if (newValue.length <= 2 && !this.checkClear) {
        this.checkClear = true;
        params.q = '';
        this.service.queryParams.next(params);
      }
    });
  }

}
